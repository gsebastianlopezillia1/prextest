import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'recover',
    loadChildren: () => import('./pages/recover/recover.module').then( m => m.RecoverPageModule)
  },
  {
    path: 'new-movie',
    loadChildren: () => import('./pages/new-movie/new-movie.module').then( m => m.NewMoviePageModule)
  },
  {
    path: 'customize',
    loadChildren: () => import('./pages/customize/customize.module').then( m => m.CustomizePageModule)
  },
  {
    path: 'update-movie/:id',
    loadChildren: () => import('./pages/update-movie/update-movie.module').then( m => m.UpdateMoviePageModule)
  },
  {
    path: 'detail-movie/:id',
    loadChildren: () => import('./pages/detail-movie/detail-movie.module').then( m => m.DetailMoviePageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
