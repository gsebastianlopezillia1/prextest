import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-recover',
  templateUrl: './recover.page.html',
  styleUrls: ['./recover.page.scss'],
})
export class RecoverPage implements OnInit {
  @ViewChild('emailInput') myInput;
  recoverForm: FormGroup;

  constructor(
    private alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private authenticationSvc: AuthenticationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.recoverForm = this.formBuilder.group({
      emailAddress: ['', [Validators.required]],
    });
    setTimeout(() => {
      this.myInput.setFocus();
    }, 150);
  }

  recover() {
    if (this.recoverForm.controls.emailAddress.value !== '') {
      this.authenticationSvc
        .PasswordRecover(this.recoverForm.controls.emailAddress.value)
        .then(() =>
          this.emailSendedAlert(this.recoverForm.controls.emailAddress.value)
        )
        .catch((err) => console.error(err));
    }
  }

  async emailSendedAlert(email) {
    const alert = await this.alertCtrl.create({
      header: 'Password restored',
      subHeader:
        'An email was sent to ' +
        email +
        ' with the steps to recover your account.',
      backdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigate(['/login']);
          },
        },
      ],
    });
    return alert.present();
  }
}
