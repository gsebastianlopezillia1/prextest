import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from 'src/app/services/movie.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detail-movie',
  templateUrl: './detail-movie.page.html',
  styleUrls: ['./detail-movie.page.scss'],
})
export class DetailMoviePage implements OnInit {
  movieId = '';
  movie = null;
  constructor(
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private movieSvc: MovieService,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.movieId = this.activatedRoute.snapshot.paramMap.get('id');
    this.movieSvc
      .getMovie(this.movieId)
      .valueChanges()
      .subscribe((movie) => (this.movie = movie));
  }

  update() {
    this.router.navigate(['/update-movie/' + this.movieId]);
  }

  async delete() {
    (
      await this.alertCtrl.create({
        header: 'Confirm',
        subHeader:
          'Do you want to delete ' +
          this.movie.movieTitle +
          ' from your movie rate list?',
        backdropDismiss: false,
        buttons: [
          {
            text: 'DELETE',
            handler: () => {
              this.movieSvc.deleteMovie(this.movieId);
              this.router.navigate(['/home']);
            },
          },
          {
            text: 'CANCEL',
          },
        ],
      })
    ).present();
  }
}
