import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from 'src/app/services/app.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  hide = true;
  hide2 = true;
  passwordValue = '';
  passwordValue2 = '';
  emailAddress = '';
  loadingBarVisible = false;
  registerError = false;
  registerForm: FormGroup;
  constructor(
    public authService: AuthenticationService,
    public formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private router: Router,
    private appService: AppService
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {
        emailAddress: ['', [Validators.required]],
        passwordValue: ['', [Validators.required, Validators.pattern(/^(?=.+?[A-Za-z])(?=.+?[0-9])(?=.+?[#?!@$%^&*-]).{8,}$/gm)]],
        passwordValue2: ['', [Validators.required]],
      },
      {
        validator: this.checkIfMatchingPasswords(
          'passwordValue',
          'passwordValue2'
        ),
      }
    );
  }

  checkIfMatchingPasswords(passwordValue: string, passwordValue2: string) {
    return (group: FormGroup) => {
      const passwordInput = group.controls[passwordValue];
      const passwordConfirmationInput = group.controls[passwordValue2];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  async register() {
    // TODO: name availability validation, name requirements, passwors requirements
    console.log(this.registerForm.controls.emailAddress.value);
    this.loadingBarVisible = true;
    this.authService
      .RegisterUser(
        this.registerForm.controls.emailAddress.value,
        this.registerForm.controls.passwordValue.value
      )
      .then((res) => {
        this.authService.SendVerificationMail();
        this.emailSendedAlert(this.registerForm.controls.emailAddress.value);
      })
      .catch((error) => {
        window.alert(error.message);
      })
      .finally(() => {
        this.loadingBarVisible = false;
      });
  }

  async emailSendedAlert(email) {
    const alert = await this.alertCtrl.create({
      header: 'Congrats! Account Created!',
      subHeader:
        'An email was sent to ' +
        email +
        ' with the steps to activate your account.',
      backdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigate(['/login']);
          },
        },
      ],
    });
    return alert.present();
  }

  errorControl() {
    return this.registerForm.controls;
  }
}
