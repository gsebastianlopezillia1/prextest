import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { UpdateMoviePageRoutingModule } from './update-movie-routing.module';

import { UpdateMoviePage } from './update-movie.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    UpdateMoviePageRoutingModule
  ],
  declarations: [UpdateMoviePage]
})
export class UpdateMoviePageModule {}
