import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from 'src/app/services/movie.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-movie',
  templateUrl: './update-movie.page.html',
  styleUrls: ['./update-movie.page.scss'],
})
export class UpdateMoviePage implements OnInit {
  movieId = '';
  movie = null;
  updateMovieForm: FormGroup;
  rating = 0;
  constructor(
    private activatedRoute: ActivatedRoute,
    private movieSvc: MovieService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.movieId = this.activatedRoute.snapshot.paramMap.get('id');
    this.movieSvc
      .getMovie(this.movieId)
      .valueChanges()
      .subscribe((movie) => {
        this.movie = movie;
        this.updateMovieForm = this.formBuilder.group({
          movieImage: [this.movie.movieImage, [Validators.required]],
          movieTitle: [this.movie.movieTitle, [Validators.required]],
          movieDesc: [this.movie.movieDesc, [Validators.required]],
          movieDate: [this.movie.movieDate, [Validators.required]],
          movieRating: [this.movie.movieRating, []],
        });
      });
  }

  setRating(rating) {
    this.updateMovieForm.controls.movieRating.setValue(rating);
    this.movie.movieRating = rating;
  }

  loadImageFromDevice(event) {
    onchange = () => {
      if (event.target.files[0].size > 2097152) {
        alert('File is too big!');
        return;
      }
    };
    return new Promise((resolve, _) => {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.movie.movieImage = reader.result;
        this.updateMovieForm.controls.movieImage.setValue(reader.result);
      };
      reader.onerror = (error) => {
        console.error('Error-loadImageFromDevice: ', error);
      };
    });
  }

  updateRate() {
    this.movieSvc.updateMovie(this.updateMovieForm.value, this.movieId);
    this.router.navigate(['/home']);
  }
}
