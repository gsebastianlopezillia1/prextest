import { Component, OnInit } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import { AppService } from 'src/app/services/app.service';
import { MenuController, AlertController } from '@ionic/angular';

import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject,
} from '@angular/fire/compat/database';
import { Movie } from 'src/app/shared/movie';
import { Observable, of, Subscription } from 'rxjs';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  movies: any[];
  loadingBarVisible = true;
  ms: AngularFireList<any>;

  constructor(private alertCtrl: AlertController,
    private movieSvc: MovieService,
    private menuCtrl: MenuController,
    private appService: AppService) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.movieSvc
      .getMoviesList()
      .snapshotChanges()
      .subscribe((res) => {
        this.movies = res.map((change) => ({
          key: change.payload.key,
          ...change.payload.val(),
        }));
        this.loadingBarVisible = false;
      });
  }

  async delete(movie) {
    this.loadingBarVisible = true;
    (
      await this.alertCtrl.create({
        header: 'Confirm',
        subHeader:
          'Do you want to delete ' +
          movie.movieTitle +
          ' from your movie rate list?',
        backdropDismiss: false,
        buttons: [
          {
            text: 'DELETE',
            handler: () => {
              this.movieSvc
      .deleteMovie(movie.key)
      .then(() => (this.loadingBarVisible = false));
            },
          },
          {
            text: 'CANCEL',
          },
        ],
      })
    ).present();
  }

  doRefresh(e) {
    this.movieSvc
      .getMoviesList()
      .snapshotChanges()
      .subscribe((res) => {
        this.movies = res.map((change) => ({
          key: change.payload.key,
          ...change.payload.val(),
        }));
        e.target.complete();
      });
  }
}
