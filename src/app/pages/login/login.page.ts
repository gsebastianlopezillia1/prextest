import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AppService } from 'src/app/services/app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('username') myInput;
  loginError = false;
  hide = true;
  loadingBarVisible = false;
  loginForm: FormGroup;
  appSvcInit: boolean;
  constructor(
    private alertCtrl: AlertController,
    public authService: AuthenticationService,
    public formBuilder: FormBuilder,
    private router: Router,
    private menuCtrl: MenuController,
    private appService: AppService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      emailAddress: ['', [Validators.required]],
      passwordValue: ['', [Validators.required]],
    });
    console.log('(*-*)', this.appService.appLogo);
  }

  async ionViewWillEnter() {
    this.appSvcInit = this.appService.initialized
      ? this.appService.initialized
      : await this.appService.initialize();
    this.menuCtrl.enable(false);
    this.authService.SignOut();
    this.myInput.setFocus();
  }

  async login() {
    this.loadingBarVisible = true;
    this.authService
      .SignIn(
        this.loginForm.controls.emailAddress.value,
        this.loginForm.controls.passwordValue.value
      )
      .then((res) => {
        if (this.authService && this.authService.isEmailVerified) {
          this.loginError = false;
          this.router.navigate(['home']);
        } else {
          this.loginError = true;
          window.alert('Email is not verified');
          return false;
        }
      })
      .catch((error) => {
        console.error('login', error);
        this.loginError = true;
        this.loginFailAlert();
      })
      .finally(() => {
        this.loadingBarVisible = false;
      });
  }

  async loginFailAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Login Failed',
      backdropDismiss: false,
      subHeader: 'An error has ocurred, :( Please try again!',
      buttons: [
        {
          text: 'OK',
        },
      ],
    });
    return alert.present();
  }
}
