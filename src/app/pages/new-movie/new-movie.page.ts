import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { MovieService } from 'src/app/services/movie.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-movie',
  templateUrl: './new-movie.page.html',
  styleUrls: ['./new-movie.page.scss'],
})
export class NewMoviePage implements OnInit {
  movieForm: FormGroup;
  chosenUrl;
  rating = 0;

  constructor(
    private alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public domSanitizer: DomSanitizer,
    private router: Router,
    public movieSvc: MovieService
  ) {}

  ngOnInit() {
    this.movieForm = this.formBuilder.group({
      movieImage: ['', [Validators.required]],
      movieTitle: ['', [Validators.required]],
      movieDesc: ['', [Validators.required]],
      movieDate: ['', [Validators.required]],
      movieRating: ['', []],
    });
  }

  newRate() {
    this.movieSvc.addMovie(this.movieForm.value);
    this.movieSavedAlert();
  }

  loadImageFromDevice(event) {
    onchange = () => {
      if (event.target.files[0].size > 2097152) {
        alert('File is too big!');
        return;
      }
    };
    return new Promise((resolve, _) => {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.chosenUrl = reader.result;
        this.movieForm.controls.movieImage.setValue(reader.result);
      };
      reader.onerror = (error) => {
        console.error('Error-loadImageFromDevice: ', error);
      };
    });
  }

  setRating(rating) {
    this.movieForm.controls.movieRating.setValue(rating);
    this.rating = rating;
  }

  async movieSavedAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Movie Saved',
      subHeader: 'Your data is now secure on the cloud.',
      backdropDismiss: false,
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.movieForm.reset();
            this.router.navigate(['/home']);
          },
        },
      ],
    });
    return alert.present();
  }
}
