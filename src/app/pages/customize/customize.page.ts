import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from 'src/app/services/app.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-customize',
  templateUrl: './customize.page.html',
  styleUrls: ['./customize.page.scss'],
})
export class CustomizePage implements OnInit {
  @ViewChild('fileInput') fileInput;
  appForm: FormGroup;
  imageSrc = '';
  constructor(
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private appSvc: AppService
  ) {}

  ngOnInit() {
    this.appForm = this.formBuilder.group({
      appLogo: [this.appSvc.appLogo, [Validators.required]],
      appName: [this.appSvc.appName, [Validators.required]],
    });
    this.imageSrc = this.appSvc.appLogo;
  }

  updateApp() {
    this.appSvc
      .setAppCustomData(
        this.appForm.controls.appLogo.value,
        this.appForm.controls.appName.value
      )
      .then(() => {
        this.valuesUpdatedAlert();
      });
  }

  loadImageFromDevice(event) {
    onchange = () => {
      if (event.target.files[0].size > 2097152) {
        alert('File is too big!');
        return;
      }
    };
    return new Promise((resolve, _) => {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageSrc = reader.result.toString();
        this.appForm.controls.appLogo.setValue(reader.result.toString());
      };
      reader.onerror = (error) => {
        console.log('Error: ', error);
      };
    });
  }

  restore() {
    this.appForm.reset();
    this.appSvc.restoreDefaultValues();
    this.fileInput.files = [];
    this.imageSrc = '../../assets/imgs/Logo-PREX-cinema.png';
    this.valuesRestoredAlert();
  }

  async valuesRestoredAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Values restored',
      backdropDismiss: false,
      subHeader: 'The App logo and name are restored to default values.',
      buttons: [
        {
          text: 'OK',
        },
      ],
    });
    return alert.present();
  }

  async valuesUpdatedAlert() {
    const alert = await this.alertCtrl.create({
      header: 'Values updated',
      backdropDismiss: false,
      subHeader: 'The App logo and name are updated as you wish.',
      buttons: [
        {
          text: 'OK',
        },
      ],
    });
    return alert.present();
  }
}
