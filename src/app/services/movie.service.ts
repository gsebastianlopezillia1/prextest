import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { Movie } from '../shared/movie';
import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject,
} from '@angular/fire/compat/database';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  movieSeed = 'PREX-MOVIE';
  moviesRef: AngularFireList<Movie>;
  movieRef: AngularFireObject<Movie>;
  constructor(private db: AngularFireDatabase) {}

  // Create Movie
  addMovie(movie: Movie) {
    this.moviesRef = this.db.list('movies-list');
    this.moviesRef.push(movie);
  }

  // Fetch Single Movie Object
  getMovie($key: string) {
    this.movieRef = this.db.object('movies-list/' + $key);
    return this.movieRef;
  }

  // Fetch Movies List
  getMoviesList() {
    this.moviesRef = this.db.list('movies-list');
    return this.moviesRef;
  }

  // Update Movie Object
  updateMovie(movie: Movie, $key) {
    this.movieRef = this.db.object('movies-list/' + $key);
    this.movieRef.update(movie);
  }

  // Delete Movie Object
  deleteMovie($key: string) {
    this.movieRef = this.db.object('movies-list/' + $key);
    return this.movieRef.remove();
  }
}
