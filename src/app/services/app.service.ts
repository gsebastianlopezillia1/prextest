import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
@Injectable({
  providedIn: 'root',
})
export class AppService {
  public appLogo = '';
  public appName = '';
  appDataSeed = 'PREX-APP-DATA';
  initialized = false;
  constructor(private storageSvc: StorageService) {}

  initialize(){
    return this.storageSvc.get(this.appDataSeed).then((data) => {
      if(data) {
        this.appLogo = JSON.parse(data).appLogo;
        this.appName = JSON.parse(data).appName;
        this.initialized = true;
      }
      return this.initialized;
    });
  }

  setAppCustomData(appLogo, appName) {
    this.appLogo = appLogo;
    this.appName = appName;
    return this.storageSvc.set(this.appDataSeed, JSON.stringify({appLogo,appName}));
  }

  restoreDefaultValues() {
    this.appLogo = '';
    this.appName = '';
    return this.storageSvc.set(this.appDataSeed, JSON.stringify({appLogo: this.appLogo, appName: this.appName}));
  }
}
