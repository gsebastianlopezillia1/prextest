import { Injectable } from '@angular/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {}

  set(key: string, value: string): Promise<boolean> {
    return Storage.set({ key, value })
      .then(() => true)
      .catch((err) => {
        console.error(err);
        return false;
      });
  }

  get(key: string) {
    return Storage.get({ key })
      .then((value) => value.value)
      .catch((err) => {
        console.error(err);
        return null;
      });
  }

  remove(key: string) {
    return Storage.remove({ key })
      .then(() => true)
      .catch((err) => {
        console.error(err);
        return false;
      });
  }

  async existKey(key: string) {
    return await Storage.keys()
      .then((keysResult) => keysResult.keys.indexOf(key) > -1)
      .catch((err) => {
        console.error(err);
        return false;
      });
  }

  async keys() {
    return await Storage.keys()
      .then((keysResult) => keysResult.keys)
      .catch((err) => {
        console.error(err);
        return false;
      });
  }
}
