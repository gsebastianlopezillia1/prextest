import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AppService } from './services/app.service';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  appName = 'PREX-Cinema';
  public appPages = [
    { title: 'Customize', url: '/customize', icon: 'brush' },
  ];
  constructor(
    public firestore: AngularFirestore,
    private authenticationSvc: AuthenticationService,
    private appSvc: AppService,
    private router: Router
  ) {}

  logout() {
    this.authenticationSvc.SignOut().then(()=>this.router.navigate(['/login']));
  }
}
