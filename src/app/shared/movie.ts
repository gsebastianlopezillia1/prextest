export interface Movie {
  $key: string;
  title: string;
  date: string;
  desc: string;
  photoURL: string;
  rating: number;
}
